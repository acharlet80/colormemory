package com.example.colormemory30;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.Random;

import java.util.ArrayList;

public class GameActivity extends AppCompatActivity {

    //déclaration des variables utiles au projet

    Random r = new Random();
    protected final int valeur = 4 + r.nextInt(7 - 4);
    protected int delai = 6000;
    protected Button btn_rouge;
    protected Button btn_vert_clair;
    protected Button btn_bleu;
    protected Button btn_noir;
    protected Button btn_vert;
    protected Button btn_orange;
    protected Button btn_orange_clair;
    protected Button btn_violet;
    protected Button btn_bleu_clair;
    protected Button btn_rouge_clair;
    protected Button btn_jaune;
    protected Button btn_marron;

    protected Drawable couleurB;
    protected Drawable couleurBC;
    Drawable couleurN;
    protected Drawable couleurO;
    Drawable couleurOC;
    Drawable couleurR;
    Drawable couleurRC;
    Drawable couleurV;
    Drawable couleurVC;
    protected Drawable couleurVi;
    Drawable couleurM;
    Drawable couleurJ;

    protected final int myColor = Color.parseColor("#AAAAAA");

    int[] CollectRand;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        btn_rouge = (Button) findViewById(R.id.btn_rouge);
        btn_rouge_clair = (Button) findViewById(R.id.btn_rouge_clair);
        btn_vert = (Button) findViewById(R.id.btn_vert);
        btn_bleu = (Button) findViewById(R.id.btn_bleu);
        btn_noir = (Button) findViewById(R.id.btn_noir);
        btn_orange = (Button) findViewById(R.id.btn_orange);
        btn_orange_clair = (Button) findViewById(R.id.btn_orange_clair);
        btn_vert_clair = (Button) findViewById(R.id.btn_vert_clair);
        btn_violet = (Button) findViewById(R.id.btn_violet);
        btn_jaune = (Button) findViewById(R.id.btn_jaune);
        btn_marron = (Button) findViewById(R.id.btn_marron);
        btn_bleu_clair = (Button) findViewById(R.id.btn_bleu_clair);


       // creation d'une liste pour tous les boutons

        ArrayList<Button> boutons = new ArrayList<>();
        boutons.add(btn_rouge);
        boutons.add(btn_jaune);
        boutons.add(btn_vert_clair);
        boutons.add(btn_noir);
        boutons.add(btn_orange);
        boutons.add(btn_bleu_clair);
        boutons.add(btn_bleu);
        boutons.add(btn_violet);
        boutons.add(btn_rouge_clair);
        boutons.add(btn_orange_clair);
        boutons.add(btn_marron);
        boutons.add(btn_vert);

        // recupere le background initial des boutons
        couleurB = btn_bleu.getBackground();
        couleurBC = btn_bleu_clair.getBackground();
        couleurN = btn_noir.getBackground();
        couleurO = btn_orange.getBackground();
        couleurOC = btn_orange_clair.getBackground();
        couleurR = btn_rouge.getBackground();
        couleurRC = btn_rouge_clair.getBackground();
        couleurV = btn_vert.getBackground();
        couleurVC = btn_vert_clair.getBackground();
        couleurVi = btn_violet.getBackground();
        couleurM = btn_marron.getBackground();
        couleurJ = btn_jaune.getBackground();



        ArrayList<Integer> collectRand = new ArrayList<Integer>();
        //verif();
//Uneclaire();

  /*      final Handler handlerX = new Handler();
        handlerX.postDelayed(new Runnable() {
            @Override
            public void run() {
                Deuxclaire();
            }
        }, 2000); */

    }

    private void Uneclaire(){

        //rends invisibles les boutons qui ne doivent pas apparaitre dans ce niveau
        btn_rouge_clair.setVisibility(Button.INVISIBLE);
        btn_vert_clair.setVisibility(Button.INVISIBLE);
        btn_vert.setVisibility(Button.INVISIBLE);
        btn_rouge.setVisibility(Button.INVISIBLE);
        btn_noir.setVisibility(Button.INVISIBLE);
        btn_marron.setVisibility(Button.INVISIBLE);
        btn_jaune.setVisibility(Button.INVISIBLE);
        btn_orange_clair.setVisibility(Button.INVISIBLE);

        Toast.makeText(getApplicationContext(), Integer.toString(valeur), Toast.LENGTH_LONG).show();


        final Handler handlerOriginal = new Handler();
        handlerOriginal.postDelayed(new Runnable() {
            @Override
            public void run() {
                switch (valeur) {
                    case 4:
                        final Handler handler4 = new Handler();
                        handler4.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                //Do something after 100ms
                                btn_orange.setBackground(couleurO);
                            }
                        }, 2000);
                        final Handler handlerE = new Handler();
                        handlerE.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                //Do something after 100ms
                                btn_orange.setBackground(new ColorDrawable(myColor));
                            }
                        }, 4000);
                        final Handler handlerr = new Handler();
                        handlerr.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                btn_orange.setBackground(couleurO);
                                btn_bleu_clair.setBackground(couleurBC);
                                btn_bleu.setBackground(couleurB);
                                btn_violet.setBackground(couleurVi);
                            }
                        }, 6000);
                        break;
                    case 5:
                        final Handler handler5 = new Handler();
                        handler5.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                //Do something after 100ms
                                btn_bleu_clair.setBackground(couleurBC);
                            }
                        }, 2000);
                        final Handler handlerF = new Handler();
                        handlerF.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                //Do something after 100ms
                                btn_bleu_clair.setBackground(new ColorDrawable(myColor));
                            }
                        }, 4000);
                        final Handler handlerrr = new Handler();
                        handlerrr.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                btn_orange.setBackground(couleurO);
                                btn_bleu_clair.setBackground(couleurBC);
                                btn_bleu.setBackground(couleurB);
                                btn_violet.setBackground(couleurVi);
                            }
                        }, 6000);
                        break;
                    case 6:
                        final Handler handler6 = new Handler();
                        handler6.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                //Do something after 100ms
                                btn_bleu.setBackground(couleurB);
                            }
                        }, 2000);
                        final Handler handlerG = new Handler();
                        handlerG.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                //Do something after 100ms
                                btn_bleu.setBackground(new ColorDrawable(myColor));
                            }
                        }, 4000);
                        final Handler handlerrrr = new Handler();
                        handlerrrr.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                btn_orange.setBackground(couleurO);
                                btn_bleu_clair.setBackground(couleurBC);
                                btn_bleu.setBackground(couleurB);
                                btn_violet.setBackground(couleurVi);
                            }
                        }, 6000);
                        break;
                    case 7:
                        final Handler handler7 = new Handler();
                        handler7.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                //Do something after 100ms
                                btn_violet.setBackground(couleurVi);
                            }
                        }, 2000);
                        final Handler handlerH = new Handler();
                        handlerH.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                //Do something after 100ms
                                btn_violet.setBackground(new ColorDrawable(myColor));
                            }
                        }, 4000);
                        final Handler handlerrrrr = new Handler();
                        handlerrrrr.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                btn_orange.setBackground(couleurO);
                                btn_bleu_clair.setBackground(couleurBC);
                                btn_bleu.setBackground(couleurB);
                                btn_violet.setBackground(couleurVi);
                            }
                        }, 6000);
                        break;
                }
            }
        }, delai);
        delai += 2000;

    }

    private void verif(){

            btn_orange.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(CollectRand[valeur] == 4){
                        Toast.makeText(getApplicationContext(), new String("OK"), Toast.LENGTH_LONG).show();
                    }
                    else{
                        Toast.makeText(getApplicationContext(), new String("PAS OK"), Toast.LENGTH_LONG).show();

                    }
                }
            });

        btn_bleu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(CollectRand[valeur] == 6){
                    Toast.makeText(getApplicationContext(), new String("OK"), Toast.LENGTH_LONG).show();
                }
                else{
                    Toast.makeText(getApplicationContext(), new String("PAS OK"), Toast.LENGTH_LONG).show();

                }
            }
        });
        btn_bleu_clair.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(CollectRand[valeur] == 5){
                    Toast.makeText(getApplicationContext(), new String("OK"), Toast.LENGTH_LONG).show();
                }
                else{
                    Toast.makeText(getApplicationContext(), new String("PAS OK"), Toast.LENGTH_LONG).show();

                }
            }
        });
        btn_violet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(CollectRand[valeur] == 7){
                    Toast.makeText(getApplicationContext(), new String("OK"), Toast.LENGTH_LONG).show();
                }
                else{
                    Toast.makeText(getApplicationContext(), new String("PAS OK"), Toast.LENGTH_LONG).show();

                }
            }
        });

    }

    private void Deuxclaire(){

        final Handler handler4 = new Handler();
        handler4.postDelayed(new Runnable() {
                                 @Override
                                 public void run() {
                                     btn_bleu.setBackground(new ColorDrawable(myColor));
                                     btn_bleu_clair.setBackground(new ColorDrawable(myColor));
                                     btn_orange.setBackground(new ColorDrawable(myColor));
                                     btn_violet.setBackground(new ColorDrawable(myColor));
                                 }
                             }, 3000);

        Toast.makeText(getApplicationContext(), Integer.toString(valeur), Toast.LENGTH_LONG).show();

for(int i=0; i<3; i++){
    final Handler handlerOriginal = new Handler();
    handlerOriginal.postDelayed(new Runnable() {
        @Override
        public void run() {
            switch (valeur) {
                case 4:
                    final Handler handler4 = new Handler();
                    handler4.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            //Do something after 100ms
                            btn_orange.setBackground(couleurO);
                        }
                    }, 4000);
                    final Handler handlerE = new Handler();
                    handlerE.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            //Do something after 100ms
                            btn_orange.setBackground(new ColorDrawable(myColor));
                        }
                    }, 6000);
                    final Handler handlerr = new Handler();
                    handlerr.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            btn_orange.setBackground(couleurO);
                            btn_bleu_clair.setBackground(couleurBC);
                            btn_bleu.setBackground(couleurB);
                            btn_violet.setBackground(couleurVi);
                        }
                    }, 8000);
                    break;
                case 5:
                    final Handler handler5 = new Handler();
                    handler5.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            //Do something after 100ms
                            btn_bleu_clair.setBackground(couleurBC);
                        }
                    }, 4000);
                    final Handler handlerF = new Handler();
                    handlerF.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            //Do something after 100ms
                            btn_bleu_clair.setBackground(new ColorDrawable(myColor));
                        }
                    }, 6000);
                    final Handler handlerrr = new Handler();
                    handlerrr.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            btn_orange.setBackground(couleurO);
                            btn_bleu_clair.setBackground(couleurBC);
                            btn_bleu.setBackground(couleurB);
                            btn_violet.setBackground(couleurVi);
                        }
                    }, 8000);
                    break;
                case 6:
                    final Handler handler6 = new Handler();
                    handler6.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            //Do something after 100ms
                            btn_bleu.setBackground(couleurB);
                        }
                    }, 4000);
                    final Handler handlerG = new Handler();
                    handlerG.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            //Do something after 100ms
                            btn_bleu.setBackground(new ColorDrawable(myColor));
                        }
                    }, 6000);
                    final Handler handlerrrr = new Handler();
                    handlerrrr.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            btn_orange.setBackground(couleurO);
                            btn_bleu_clair.setBackground(couleurBC);
                            btn_bleu.setBackground(couleurB);
                            btn_violet.setBackground(couleurVi);
                        }
                    }, 8000);
                    break;
                case 7:
                    final Handler handler7 = new Handler();
                    handler7.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            //Do something after 100ms
                            btn_violet.setBackground(couleurVi);
                        }
                    }, 4000);
                    final Handler handlerH = new Handler();
                    handlerH.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            //Do something after 100ms
                            btn_violet.setBackground(new ColorDrawable(myColor));
                        }
                    }, 6000);
                    final Handler handlerrrrr = new Handler();
                    handlerrrrr.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            btn_orange.setBackground(couleurO);
                            btn_bleu_clair.setBackground(couleurBC);
                            btn_bleu.setBackground(couleurB);
                            btn_violet.setBackground(couleurVi);
                        }
                    }, 8000);
                    break;
            }
        }
    }, delai);
    delai += 2000;
}
}



}


























          /*  final Handler handlerOriginal = new Handler();
            handlerOriginal.postDelayed(new Runnable() {
                @Override
                public void run() {
                    switch (valeur) {
                        case 0:
                            final Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    //Do something after 100ms
                                    btn_rouge.setBackground(couleurR);
                                }
                            }, 2000);

                            final Handler handlerA = new Handler();
                            handlerA.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    //Do something after 100ms
                                    btn_rouge.setBackground(new ColorDrawable(myColor));
                                }
                            }, 4000);
                            break;

                        case 1:
                            final Handler handler1 = new Handler();
                            handler1.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    //Do something after 100ms
                                    btn_jaune.setBackground(couleurJ);
                                }
                            }, 2000);
                            final Handler handlerB = new Handler();
                            handlerB.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    //Do something after 100ms
                                    btn_jaune.setBackground(new ColorDrawable(myColor));
                                }
                            }, 4000);
                            break;

                        case 2:
                            final Handler handler2 = new Handler();
                            handler2.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    //Do something after 100ms
                                    btn_vert_clair.setBackground(couleurVC);
                                }
                            }, 2000);
                            final Handler handlerC = new Handler();
                            handlerC.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    //Do something after 100ms
                                    btn_vert_clair.setBackground(new ColorDrawable(myColor));
                                }
                            }, 4000);
                            break;

                        case 3:
                            final Handler handler3 = new Handler();
                            handler3.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    //Do something after 100ms
                                    btn_noir.setBackground(couleurN);
                                }
                            }, 2000);
                            final Handler handlerD = new Handler();
                            handlerD.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    //Do something after 100ms
                                    btn_noir.setBackground(new ColorDrawable(myColor));
                                }
                            }, 4000);
                            break;
                        case 4:
                            final Handler handler4 = new Handler();
                            handler4.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    //Do something after 100ms
                                    btn_orange.setBackground(couleurO);
                                }
                            }, 2000);
                            final Handler handlerE = new Handler();
                            handlerE.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    //Do something after 100ms
                                    btn_orange.setBackground(new ColorDrawable(myColor));
                                }
                            }, 4000);
                            break;
                        case 5:
                            final Handler handler5 = new Handler();
                            handler5.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    //Do something after 100ms
                                    btn_bleu_clair.setBackground(couleurBC);
                                }
                            }, 2000);
                            final Handler handlerF = new Handler();
                            handlerF.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    //Do something after 100ms
                                    btn_bleu_clair.setBackground(new ColorDrawable(myColor));
                                }
                            }, 4000);
                            break;
                        case 6:
                            final Handler handler6 = new Handler();
                            handler6.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    //Do something after 100ms
                                    btn_bleu.setBackground(couleurB);
                                }
                            }, 2000);
                            final Handler handlerG = new Handler();
                            handlerG.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    //Do something after 100ms
                                    btn_bleu.setBackground(new ColorDrawable(myColor));
                                }
                            }, 4000);
                            break;
                        case 7:
                            final Handler handler7 = new Handler();
                            handler7.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    //Do something after 100ms
                                    btn_violet.setBackground(couleurVi);
                                }
                            }, 2000);
                            final Handler handlerH = new Handler();
                            handlerH.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    //Do something after 100ms
                                    btn_violet.setBackground(new ColorDrawable(myColor));
                                }
                            }, 4000);
                            break;
                        case 8:
                            final Handler handler8 = new Handler();
                            handler8.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    //Do something after 100ms
                                    btn_rouge_clair.setBackground(couleurRC);
                                }
                            }, 2000);

                            final Handler handlerI = new Handler();
                            handlerI.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    //Do something after 100ms
                                    btn_rouge_clair.setBackground(new ColorDrawable(myColor));
                                }
                            }, 4000);
                            break;
                        case 9:
                            final Handler handler9 = new Handler();
                            handler9.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    //Do something after 100ms
                                    btn_orange_clair.setBackground(couleurOC);
                                }
                            }, 2000);

                            final Handler handlerJ = new Handler();
                            handlerJ.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    //Do something after 100ms
                                    btn_orange_clair.setBackground(new ColorDrawable(myColor));
                                }
                            }, 4000);
                            break;
                        case 10:
                            final Handler handler10 = new Handler();
                            handler10.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    //Do something after 100ms
                                    btn_marron.setBackground(couleurM);
                                }
                            }, 2000);
                            final Handler handlerK = new Handler();
                            handlerK.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    //Do something after 100ms
                                    btn_marron.setBackground(new ColorDrawable(myColor));
                                }
                            }, 4000);
                            break;
                        case 11:
                            final Handler handler11 = new Handler();
                            handler11.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    //Do something after 100ms
                                    btn_vert.setBackground(couleurV);
                                }
                            }, 2000);
                            final Handler handlerL = new Handler();
                            handlerL.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    //Do something after 100ms
                                    btn_vert.setBackground(new ColorDrawable(myColor));
                                }
                            }, 4000);
                            break;
                    }
                }
            }, delai);
            delai += 2000;
*/



