package com.example.colormemory30;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;


public class MainActivity extends AppCompatActivity {

    private Button btn_facile;
    private Button btn_difficile;
    private Button btn_expert;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btn_facile = (Button) findViewById(R.id.btn_facile);
        btn_difficile = (Button) findViewById(R.id.btn_difficile);
        btn_expert = (Button) findViewById(R.id.btn_expert);

        btn_facile.setOnClickListener(new View.OnClickListener() {
        @Override
            public void onClick(View v) {
                //on creer une nouvelle intent on definit la class de depart ici this et la class d'arrivé ici SecondActivite
                Intent intent=new Intent(MainActivity.this,FacileActivity.class);
                //on lance l'intent, cela a pour effet de stoper l'activité courante et lancer une autre activite ici SecondActivite
                startActivity(intent);

            }
        });

        btn_difficile.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            //on creer une nouvelle intent on definit la class de depart ici this et la class d'arrivé ici SecondActivite
            Intent intent=new Intent(MainActivity.this,DifficileActivity.class);
            //on lance l'intent, cela a pour effet de stoper l'activité courante et lancer une autre activite ici SecondActivite
            startActivity(intent);

        }
    });

        btn_expert.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            //on creer une nouvelle intent on definit la class de depart ici this et la class d'arrivé ici SecondActivite
            Intent intent=new Intent(MainActivity.this,ExpertActivity.class);
            //on lance l'intent, cela a pour effet de stoper l'activité courante et lancer une autre activite ici SecondActivite
            startActivity(intent);

        }
    });
}
}
