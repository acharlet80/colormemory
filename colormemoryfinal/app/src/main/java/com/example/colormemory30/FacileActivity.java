package com.example.colormemory30;

import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.widget.Button;

public class FacileActivity extends GameActivity{

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_game);

            btn_rouge = (Button) findViewById(R.id.btn_rouge);
            btn_rouge_clair = (Button) findViewById(R.id.btn_rouge_clair);
            btn_vert = (Button) findViewById(R.id.btn_vert);
            btn_bleu = (Button) findViewById(R.id.btn_bleu);
            btn_noir = (Button) findViewById(R.id.btn_noir);
            btn_orange = (Button) findViewById(R.id.btn_orange);
            btn_orange_clair = (Button) findViewById(R.id.btn_orange_clair);
            btn_vert_clair = (Button) findViewById(R.id.btn_vert_clair);
            btn_violet = (Button) findViewById(R.id.btn_violet);
            btn_jaune = (Button) findViewById(R.id.btn_jaune);
            btn_marron = (Button) findViewById(R.id.btn_marron);
            btn_bleu_clair = (Button) findViewById(R.id.btn_bleu_clair);

            // defini le bacground des boutons sur gris

            btn_bleu.setBackground(new ColorDrawable(myColor));
            btn_bleu_clair.setBackground(new ColorDrawable(myColor));
            btn_noir.setBackground(new ColorDrawable(myColor));
            btn_orange.setBackground(new ColorDrawable(myColor));
            btn_orange_clair.setBackground(new ColorDrawable(myColor));
            btn_rouge_clair.setBackground(new ColorDrawable(myColor));
            btn_vert.setBackground(new ColorDrawable(myColor));
            btn_vert_clair.setBackground(new ColorDrawable(myColor));
            btn_violet.setBackground(new ColorDrawable(myColor));
            btn_marron.setBackground(new ColorDrawable(myColor));
            btn_jaune.setBackground(new ColorDrawable(myColor));
            btn_rouge.setBackground(new ColorDrawable(myColor));


            Niveau1F();
        }

        public void Niveau1F(){

            //rends invisibles les boutons qui ne doivent pas apparaitre dans ce niveau

            btn_rouge_clair.setVisibility(Button.INVISIBLE);
            btn_vert_clair.setVisibility(Button.INVISIBLE);
            btn_vert.setVisibility(Button.INVISIBLE);
            btn_rouge.setVisibility(Button.INVISIBLE);
            btn_noir.setVisibility(Button.INVISIBLE);
            btn_marron.setVisibility(Button.INVISIBLE);
            btn_jaune.setVisibility(Button.INVISIBLE);
            btn_orange_clair.setVisibility(Button.INVISIBLE);

                final Handler handlerOriginal = new Handler();
                handlerOriginal.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        switch (valeur) {
                            case 4:
                                final Handler handler4 = new Handler();
                                handler4.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        //Do something after 100ms
                                        btn_orange.setBackground(couleurO);
                                    }
                                }, 2000);
                                final Handler handlerE = new Handler();
                                handlerE.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        //Do something after 100ms
                                        btn_orange.setBackground(new ColorDrawable(myColor));
                                    }
                                }, 4000);
                                final Handler handlerr = new Handler();
                                handlerr.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        btn_orange.setBackground(couleurO);
                                        btn_bleu_clair.setBackground(couleurBC);
                                        btn_bleu.setBackground(couleurB);
                                        btn_violet.setBackground(couleurVi);
                                    }
                                }, 6000);
                                break;
                            case 5:
                                final Handler handler5 = new Handler();
                                handler5.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        //Do something after 100ms
                                        btn_bleu_clair.setBackground(couleurBC);
                                    }
                                }, 2000);
                                final Handler handlerF = new Handler();
                                handlerF.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        //Do something after 100ms
                                        btn_bleu_clair.setBackground(new ColorDrawable(myColor));
                                    }
                                }, 4000);
                                final Handler handlerrr = new Handler();
                                handlerrr.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        btn_orange.setBackground(couleurO);
                                        btn_bleu_clair.setBackground(couleurBC);
                                        btn_bleu.setBackground(couleurB);
                                        btn_violet.setBackground(couleurVi);
                                    }
                                }, 6000);
                                break;
                            case 6:
                                final Handler handler6 = new Handler();
                                handler6.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        //Do something after 100ms
                                        btn_bleu.setBackground(couleurB);
                                    }
                                }, 2000);
                                final Handler handlerG = new Handler();
                                handlerG.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        //Do something after 100ms
                                        btn_bleu.setBackground(new ColorDrawable(myColor));
                                    }
                                }, 4000);
                                final Handler handlerrrr = new Handler();
                                handlerrrr.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        btn_orange.setBackground(couleurO);
                                        btn_bleu_clair.setBackground(couleurBC);
                                        btn_bleu.setBackground(couleurB);
                                        btn_violet.setBackground(couleurVi);
                                    }
                                }, 6000);
                                break;
                            case 7:
                                final Handler handler7 = new Handler();
                                handler7.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        //Do something after 100ms
                                        btn_violet.setBackground(couleurVi);
                                    }
                                }, 4000);
                                final Handler handlerH = new Handler();
                                handlerH.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        //Do something after 100ms
                                        btn_violet.setBackground(new ColorDrawable(myColor));
                                    }
                                }, 6000);
                                final Handler handlerrrrr = new Handler();
                                handlerrrrr.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        btn_orange.setBackground(couleurO);
                                        btn_bleu_clair.setBackground(couleurBC);
                                        btn_bleu.setBackground(couleurB);
                                        btn_violet.setBackground(couleurVi);
                                    }
                                }, 6000);
                                break;
                        }
                    }
                }, delai);
                delai += 2000;
        }
}


